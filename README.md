# Angeliza Take Home Assigment

## Development server

Run `npm run-script start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `npm run-script ng generate component component-name` to generate a new component. You can also use `npm run-script ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Task

One of the major features that we have launched in the Connect platform within the last year is LevelUp, a gamification framework embedded in Connect.

### Background on LevelUp

A LevelUp Campaign is a custom gamification setup that allows you to drive user engagement and retention by awarding users with skill-specific experience points (XP) and achievements for completing actions throughout the Connect platform, such as answering surveys, reading PDFs or engaging with social feed posts.

A LevelUp Skill is a Level progression track where users can earn XP and Achievements in a particular skill. For example, a Skill could be "Trivia knowledge" in a "Chrismas Party" Campaign, where the user can track their XP, Level progress and Achievements specific to Trivia-related activities available in the Connect platform set up for the "Christmas Party" Campaign.

Levels can be used to break up the XP progression into manageable chunks and serves as achievable goals for the user either in the overall Campaign or in a specific Skill.

LevelUp Achievements one-time rewards that the user unlocks by accomplishing a particularly meaningful goal (e.g., "You've read all the important PDFs!").

One of the most important ways in which the end-users interact with LevelUp and track their progress is with Connect's Modular Dashboards.

Modular dashboards are dynamic dashboards built using different sized 'tiles' (building block components) configured by our project managers to show information for, e.g., a specfic LevelUp Campaign.

### Instructions

Your task is to build a mock Modular Dashboard with three different LevelUp tiles.

Below is screenshots of wireframes for three tiles. The three tiles are showing LevelUp Campaign information. One with little detail, one with Campaign and Sskills and the last with the Campaign, Skills and Achievements.

![Campaign progress wireframe](/readme-assets/campaign-progress-wireframe.png)

![Campaign and Skill progress wireframe](/readme-assets/campaign-and-skill-progress-wireframe.png)

![Campaign, Skill and Achievement progress wireframe](/readme-assets/campaign-skill-and-achievements-progress-wireframe.png)

In app.component.ts you will find declarations of Campaign, Skills and Achievements which can be used as the base data for these components.

It’s important to note that the above wireframes are just that, wireframes, and don’t need to be implemented pixel perfect. Below, you will find screenshots of wireframes (left) and implementations (right) of two components called skill-barometer and campaign-info. These are attached to give you inspiration as to how closely we usually follow wireframes.

![Campaign, Skill and Achievement progress wireframe](/readme-assets/skill-barometer-wireframe.png)
![Campaign, Skill and Achievement progress wireframe](/readme-assets/skill-barometer-example-implementation.png)

![Campaign, Skill and Achievement progress wireframe](/readme-assets/campaign-info-wireframe.png)
![Campaign, Skill and Achievement progress wireframe](/readme-assets/campaign-info-example-implementation.png)

With this assignment we hope that you’ll be inspired to implement the UI for these components, and try to see if you can create reusable components, for instance the "campaign and skill" wireframe might be reused for the left hand side of the "campaign, skill and achievements" wireframe. Likewise it might make sense to create a progress-bar component that encapsulates the UI and possible logic for that.

We use Tailwind a lot and usually end up adding very little stuff in the components .scss files. We use the `tw-` prefix in tailwind, meaning you’ll need to use the class `tw-flex` instead of `flex` to make an element `display: flex`. We usually use font awesome icons, but left it out of the repository, as to not include too many “moving parts”.

Don’t worry to much about the style that we already added to this repository, it has only been included because we hope that you will find it useful, and not because you need keep it.

## Helpful notes

- Use Angular Material components and Tailwind.
- For the purposes of this task, the whole configuration aspect of both LevelUp and Modular Dashboards is naturally irrelevant. The content in your app can be static, as long as if we change the content of the mock data provided in app.component.ts, it is reflected in your app when it reloads.
- Consider reusability of components
- We understand that you may have time constraints due to your current job, moving, etc. The most important aspect for us is not the completion of every aspect of the task, but having a foundation for a follow-up discussion about your code and decisions.
- While we've given you an example of how the circular barometer looks, it is not a part of the task to build one.
- Communication is essential in our team. If anything is unclear or if you have any questions, please let us know. We consider it a strength, not a weakness, for you to reach out. Please keep in mind that we may not be able to reply immediately.

## Deadline

Thursday the 7th at 9 AM.

Email us with a link to your repository when you have finished your work.
