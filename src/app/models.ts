export interface LevelUpCampaign {
  id: string;
  title: string;
  description: string;
  image: string;
}

export interface LevelUpSkill {
  id: string;
  title: string;
  image: string;

  currentXp: number;

  currentLevel: LevelUpSkillLevel;
  nextLevel?: LevelUpSkillLevel;
};

export interface LevelUpSkillLevel {
  title: string;
  totalXpRequirement: number;
}

export interface LevelUpAchievement {
  id: string;
  title: string;
  skillName: string;
  unlocked: boolean;
  requiredProgress: number;
  currentProgress: number;
  image: string;
}
