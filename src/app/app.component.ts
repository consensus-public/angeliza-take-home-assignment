import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { CampaignProgressSmallComponent } from './campaign-progress-small/campaign-progress-small.component';
import { LevelUpAchievement, LevelUpCampaign, LevelUpSkill } from './models';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    MatCardModule,
    CampaignProgressSmallComponent,
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly cardStyle = {
    backgroundColor: '#FFFFFF73',
    color: '#001965',
    backdropFiler: 'blur(15px)',
  };

  readonly campaign: LevelUpCampaign = {
    id: 'campaign-1',
    title: 'My First Campaign',
    description: 'A bit longer description which might contain newlines and such, but no rich text\n\nI\'m below the rest of the text.',
    image: 'https://placehold.co/360x360/FAD/FFF',
  };

  readonly skills: LevelUpSkill[] = [
    {
      id: 'skill-1',
      title: 'Coding',
      currentXp: 55,
      image: 'https://placehold.co/360x360/EA7/FFF',
      currentLevel: {
	      title: 'Level 2',
	      totalXpRequirement: 50,
      },
      nextLevel: {
	      title: 'Level 3',
	      totalXpRequirement: 100,
      }
    },
    {
      id: 'skill-2',
      title: 'Social',
      currentXp: 155,
      image: 'https://placehold.co/360x360/ABC/FFF',
      currentLevel: {
	      title: 'Level 5',
	      totalXpRequirement: 150,
      },
    },
    {
      id: 'skill-3',
      title: 'Whatever',
      currentXp: 46,
      image: 'https://placehold.co/360x360/902/FFF',
      currentLevel: {
	      title: 'Level 1',
	      totalXpRequirement: 0,
      },
      nextLevel: {
	      title: 'Level 2',
	      totalXpRequirement: 50,
      },
    },
  ];

  readonly achievements: LevelUpAchievement[] = [
    {
      id: 'achievement-1',
      title: 'Awesome Achievement',
      skillName: 'Social',
      unlocked: true,
      requiredProgress: 5,
      currentProgress: 7,

      image: 'https://placehold.co/360x360/08a/FFF',
    },
    {
      id: 'achievement-2',
      title: 'Sporty Achievement',
      skillName: 'Whatever',
      unlocked: false,
      currentProgress: 2,
      requiredProgress: 5,

      image: 'https://placehold.co/360x360/c38/FFF',
    }
  ];
}
