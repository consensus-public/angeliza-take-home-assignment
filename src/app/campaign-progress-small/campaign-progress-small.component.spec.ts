import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignProgressSmallComponent } from './campaign-progress-small.component';

describe('CampaignProgressSmallComponent', () => {
  let component: CampaignProgressSmallComponent;
  let fixture: ComponentFixture<CampaignProgressSmallComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CampaignProgressSmallComponent]
    });
    fixture = TestBed.createComponent(CampaignProgressSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
