import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LevelUpCampaign } from '../models';

@Component({
  selector: 'app-campaign-progress-small',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './campaign-progress-small.component.html',
  styleUrls: ['./campaign-progress-small.component.scss']
})
export class CampaignProgressSmallComponent {
  @Input({ required: true }) campaign!: LevelUpCampaign;
}
